import React, { ChangeEvent, useEffect, useState } from "react";
import {
  AppBar,
  Box,
  FormControl,
  IconButton,
  Input,
  InputAdornment,
  Toolbar,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import StarBorderIcon from "@mui/icons-material/StarBorder";
import { useQuery } from "react-query";
import { Octokit } from "octokit";

import "./Home.scss";

import UserList from "../../components/UserList/UserList";

export default function Home() {
  const octokit = new Octokit();
  const [userInput, setUserInput] = useState("");
  const [usernameQueryParam, setUsernameQueryParam] = useState("");

  const onChangeUserInput = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setUserInput(event.target.value);
  };

  const { data, refetch } = useQuery({
    queryKey: ["users", usernameQueryParam],
    queryFn: async ({ queryKey }) => {
      const username = queryKey[1];
      if (!username) return [];
      const { data } = await octokit.rest.users.list({
        username,
      });
      return data;
    },
  });

  useEffect(() => {
    const inputTimer = setTimeout(() => {
      if (userInput.length < 3) return;
      console.log("Request send");
      setUsernameQueryParam(userInput);
      refetch();
    }, 1500);

    return () => clearTimeout(inputTimer);
  }, [userInput]);

  return (
    <div className="home">
      <div className="home__header">
        <Box sx={{ flexGrow: 1 }}>
          <AppBar className="home__appbar" position="static">
            <Toolbar className="home__toolbar">
              <FormControl sx={{ m: 1, width: "40%" }} variant="standard">
                <Input
                  value={userInput}
                  onChange={onChangeUserInput}
                  placeholder="Search got GitHub users..."
                  disableUnderline={true}
                  startAdornment={
                    <InputAdornment position="start">
                      <SearchIcon />
                    </InputAdornment>
                  }
                />
              </FormControl>
              <IconButton size="large" edge="start" sx={{ mr: 2 }}>
                <StarBorderIcon />
              </IconButton>
            </Toolbar>
          </AppBar>
        </Box>
      </div>
      <div className="home__content">
        {data?.length === 0 ? (
          <span>No search results...</span>
        ) : (
          <UserList classNames="home__user-list" users={data} />
        )}
      </div>
    </div>
  );
}
